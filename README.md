# LTE_Guidelines

This repository provides guidelines for:
- Data Management
- Instruments Documentation 
- Computing Infrastracture
- Good Programming Practices at LTE

## Introductions 
[Welcome to EPFL](WelcomeEPFL.md)

[Welcome to LTE](WelcomeLTE.md)

[Introduction to GitLab](GitLab/Introduction_GitLab.pdf)

## Data Management Guidelines  
- [AWS](DataManagement/DataManagement_AWS.md)

- [MXPol](DataManagement/DataManagement_MXPol.md)

- [PolXBear](DataManagement/DataManagement_PolXBear.md)

- [MRR2](DataManagement/DataManagement_MRR-2.md)

- [MRR-PRO](DataManagement/DataManagement_MRR-PRO.md)

- [WProf](DataManagement/DataManagement_WProf.md)

- [Radiometers](DataManagement/DataManagement_Radiometers.md)

- [MASC](DataManagement/DataManagement_MASC.md)

- [Parsivel](DataManagement/DataManagement_Parsivel.md) TODO

- [2DVD](DataManagement/DataManagement_2DVD.md) TODO

## Documentation related to the instruments 

- [AWS](Docs/AWS)

- [MRR](Docs/MRR)

- [MXPol](Docs/MXPol)

- [PolXBear](Docs/PolXBear)  TODO

- [Radiometers](Docs/Radiometers)

- [2DVD](Docs/2DVD)   TODO

- [Parsivel](Docs/Parsivel)   TODO

- [WProf](Docs/Parsivel)   TODO

- [MASC](Docs/Parsivel)  TODO

## Code snippets for the instruments 

- [MXPol](Code/MXPol)

- [PolXBear](Code/PolXBear)

- [MCH](Code/MCH)

- [MRR](Code/MRR)

- [WProf](Code/WProf)

- [Parsivel](Code/Parsivel)

- [2DVD](Code/2DVD)

- [MASC](Code/MASC)



