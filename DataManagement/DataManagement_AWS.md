# AWS 

Automatic Weather Station 

AWS Vaisala WXT536

AWS data are usually located in the subfolder `/AWS` of the `<campaign_name>` folder `/ltedata/<campaign_name>/AWS`.

Each `/AWS` folder contain the following subfolders: 

`/Raw_data`
- Data hierarchy: `/YYYY-MM-DD.log`

 
 
If AWS is deployed together with other instruments such MRR, pyranometer and pyrgeometer, the AWS data are located in the subfolder `/AWS` of `/ltedata/<campaign_name>/remote_camp_<XX>/AWS`.