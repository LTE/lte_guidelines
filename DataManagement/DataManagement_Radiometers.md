# Radiometers 

Pyrgeometer and Pyranometer 

 
Radiometers data are usually located in the subfolder `/Radiometers` of the `<campaign_name>` folder `/ltedata/<campaign_name>/Radiometers`.

Each `/Radiometers` folder contain the following subfolders: 

`/Raw_data`
- Data hierarchy: `/<YYYY-MM-DD>.log`
 
 
If Radiometers are deployed together with other instruments such MRR and AWS, data are located in the subfolder `/Radiometers` of `/ltedata/<campaign_name>/remote_camp_<XX>/Radiometers`.