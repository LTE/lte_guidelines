### 2DVD Data Management

This document summarizes how 2DVD data should be saved and documented on `/ltedata`. 
2DVD data are located in the subfolder `/2DVD` of the `<campaign_name>` folder: `/ltedata/<campaign_name>/2DVD/`.
 
Each `/2DVD` folder contains the following subfolders: 