
# Welcome to EPFL 

Congratulations on becoming a member of the EPFL community! In this document is an overview of useful services and resources from EPFL. 
For additional informations regarding the lab, please check the document [Welcome to LTE](WelcomeLTE.md).

### What to do in the beginning:
- Validate your [Camipro Card](https://www.epfl.ch/campus/services/camipro/). This will allow to access EPFL buildings.
- Charge with money your Camipro to pay quickly at the EPFL restaurants.
- Get your [SBB CFF FFS discount](https://www.epfl.ch/campus/mobility/public-transport/) for public transport.
- Activate your [EPFL library](https://www.epfl.ch/campus/library/) account.
- Have a look at EPFL subsidized programs: [PubliBike](https://www.epfl.ch/campus/mobility/bike/publibike-lausanne-en/), [Mobility](https://www.epfl.ch/campus/mobility/vehicles/mobility-carsharing/), [Green Motion](https://www.epfl.ch/campus/services/camipro/additional_services/green_motion_electrical_vehicle_charging/).

#### EPFL online services

| Name      | Use                                                                                                                            | Access                                      | VPN |
|-----------|--------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------|-----|
| Sesame2   | Annual tax/income statement, monthly income statement, travel requests, travel reimbursements, absences (leave, illness)       | https://sesame2-ghp.epfl.ch/                        | Yes |
| SBB EPFL  | Train tickets for work related travel                                                                                          | https://ticketshop.epfl.ch/cgi-bin/cff      | No  |
| MyCamipro | Money charge, check balance, transactions. Access to subsidised programs: PubliBike, Mobility, EPFL BBQ, Parking, Green Motion | https://mycamipro.epfl.ch/client/serhome    | No  |
| Repro     | Poster printing service                                                                                                        | https://www.epfl.ch/campus/services/repro/  | No  |



### Useful resources:
- [PolyDoc](https://www.epfl.ch/campus/associations/list/polydoc/)
- [EPFL sports](https://www.epfl.ch/campus/sports/en/sports/)
- [Point Vélo](https://www.epfl.ch/campus/mobility/fr/velo/pointvelo/)
- [Postdoc Association](https://www.epfl.ch/campus/associations/list/epda/)
- [Acide](https://acide.epfl.ch/)

