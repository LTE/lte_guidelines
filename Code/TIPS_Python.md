## Some TIPS for python  !

 Radar processing:
- pyart
- pyrad
- pyjacopo (https://gitlab.epfl.ch/wolfensb/pyjacopo), 
- pyWprof (https://gitlab.epfl.ch/praz/pyWprof)

Math/Vectorization:
- numpy
- xarray

Image analysis:
- scikit-image
- scipy

Machine learning: 
- scikit-learn
- tensorflow